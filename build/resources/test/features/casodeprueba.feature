# language: es
Característica: Verificacion de servicio Deudores Mi Covinoc

  Antecedentes: Consumir servicio autenticar

    Dado que se desea consumir el servicio para autenticarse

  Esquema del escenario:Servicio tipos de documentos Mi Covinoc
    Cuando Ejecuto servicio verificar con tipo "<tipo>"
    Entonces Valido respuesta del servicio "<estado>"

    Ejemplos:
      | tipo    | estado |
      | exitoso | 200    |
      | fallido | 401    |

  Esquema del escenario:Servicio datos de contacto Mi Covinoc
    Cuando Ejecuto servicio verificar con tipo "<tipo>" tipoiden: "<tipoiden>" documento "<documento>" campo1 "<campo1>" campo2 "<campo2>"
    Entonces Valido respuesta del servicio "<estado>"

    Ejemplos:
      | tipo    | tipoiden | documento | campo1                  | campo2               | estado |
      | exitoso | C        | 1         | consdirec,telefono,tipo | consdirec,email,tipo | 200    |
      | exitoso |          | 1         | consdirec,telefono,tipo | consdirec,email,tipo | 400    |
      | exitoso | C        |           | consdirec,telefono,tipo | consdirec,email,tipo | 400    |
      | exitoso | C        | 1         |                         | consdirec,email,tipo | 400    |
      | exitoso | C        | 1         | consdirec,telefono,tipo |                      | 400    |
      | fallido |          |           |                         |                      | 401    |

  Esquema del escenario:Servicio codigos verificacion Mi Covinoc
    Cuando Ejecuto servicio verificar con tipo "<tipo>" tipoiden: "<tipoiden>" documento "<documento>" idContacto "<idContacto>"
    Entonces Valido respuesta del servicio "<estado>"

    Ejemplos:
      | tipo    | tipoiden | documento | idContacto | estado |
      | exitoso |          | 1         | 1          | 400    |
      | exitoso | C        |           | 1          | 400    |
      | exitoso | C        | 1         |            | 400    |
      | exitoso | C        | 1         | 1          | 400    |
  @Ejecutar
  Esquema del escenario:Serviciotitulares Mi Covinoc
    Cuando Ejecuto servicio verificar con tipo "<tipo>" tipoiden: "<tipoiden>" documento "<documento>" campos1 "<campos1>" campos2 "<campos2>" campos3 "<campos3>" campos4 "<campos4>" campos5 "<campos5>"
    Entonces Valido respuesta del servicio "<estado>"

    Ejemplos:
      | tipo    | tipoiden | documento | campos1                                             | campos2                                      | campos3                        | campos4                            | campos5                                    | estado | mensaje                    |
      | exitoso | C        | 1         | nit,nombres,apellidos,tipo_iden,tipo_identificacion | telefono,tipo,nombredpto,nomciudad,tipo_real | direccion,nombredpto,nomciudad | consdocdeu,nrodoc,valor_obligacion | conscompromi,fechacompro,valop,nota,estado | 200    |                            |
      | exitoso |          | 1         | nit,nombres,apellidos,tipo_iden,tipo_identificacion | telefono,tipo,nombredpto,nomciudad,tipo_real | direccion,nombredpto,nomciudad | consdocdeu,nrodoc,valor_obligacion | conscompromi,fechacompro,valop,nota,estado | 400    | Faltan campos obligatorios |
      | exitoso | C        |           | nit,nombres,apellidos,tipo_iden,tipo_identificacion | telefono,tipo,nombredpto,nomciudad,tipo_real | direccion,nombredpto,nomciudad | consdocdeu,nrodoc,valor_obligacion | conscompromi,fechacompro,valop,nota,estado | 400    | Faltan campos obligatorios |
      | exitoso | C        | 1         |                                                     | telefono,tipo,nombredpto,nomciudad,tipo_real | direccion,nombredpto,nomciudad | consdocdeu,nrodoc,valor_obligacion | conscompromi,fechacompro,valop,nota,estado | 400    | Faltan campos obligatorios |
      | exitoso | C        | 1         | nit,nombres,apellidos,tipo_iden,tipo_identificacion |                                              | direccion,nombredpto,nomciudad | consdocdeu,nrodoc,valor_obligacion | conscompromi,fechacompro,valop,nota,estado | 400    | Faltan campos obligatorios |
      | exitoso | C        | 1         | nit,nombres,apellidos,tipo_iden,tipo_identificacion | telefono,tipo,nombredpto,nomciudad,tipo_real |                                | consdocdeu,nrodoc,valor_obligacion | conscompromi,fechacompro,valop,nota,estado | 400    | Faltan campos obligatorios |
      | exitoso | C        | 1         | nit,nombres,apellidos,tipo_iden,tipo_identificacion | telefono,tipo,nombredpto,nomciudad,tipo_real | direccion,nombredpto,nomciudad |                                    | conscompromi,fechacompro,valop,nota,estado | 200    | Faltan campos obligatorios |
      | exitoso | C        | 1         | nit,nombres,apellidos,tipo_iden,tipo_identificacion | telefono,tipo,nombredpto,nomciudad,tipo_real | direccion,nombredpto,nomciudad | consdocdeu,nrodoc,valor_obligacion |                                            | 200    | Faltan campos obligatorios |