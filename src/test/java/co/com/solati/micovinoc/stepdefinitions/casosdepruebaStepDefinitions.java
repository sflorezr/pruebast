package co.com.solati.micovinoc.stepdefinitions;

import co.com.solati.micovinoc.questions.CodigoRespuesta;
import co.com.solati.micovinoc.tasks.*;

import cucumber.api.java.ast.Cuando;
import cucumber.api.java.es.Dado;
import cucumber.api.java.es.Entonces;


import static net.serenitybdd.screenplay.GivenWhenThen.seeThat;
import static net.serenitybdd.screenplay.actors.OnStage.*;
import static org.hamcrest.CoreMatchers.equalTo;

public class casosdepruebaStepDefinitions {


    @Entonces("^Valido respuesta del servicio \"([^\"]*)\"$")
    public void validoRespuestaDelServicio(String resultado) throws InterruptedException {
        theActorCalled("robotdatos").should(seeThat("Codigo de respuesta", CodigoRespuesta.fue(),equalTo(Integer.parseInt(resultado))));
    }
    @Cuando("^Ejecuto servicio verificar con tipo \"([^\"]*)\"$")
    public void ejecutoServicioVerificarConTipo(String tipo) {
        theActorCalled("robotdatos").attemptsTo(consumeDatosInicialesTask.conTipo(tipo));

    }
    @Cuando("^Ejecuto servicio verificar con tipo \"([^\"]*)\" tipoiden: \"([^\"]*)\" documento \"([^\"]*)\" campo1 \"([^\"]*)\" campo2 \"([^\"]*)\"$")
    public void ejecutoServicioVerificarConTipoTipoidenDocumentoCampoCampo(String tipo, String tipoiden, String documento, String campo1, String campo2) {
        theActorCalled("robotdatos").attemptsTo(consumeDatosContactoTask.conDatos(tipo,tipoiden,documento,campo1,campo2));
    }
    @Cuando("^Ejecuto servicio verificar con tipo \"([^\"]*)\" tipoiden: \"([^\"]*)\" documento \"([^\"]*)\" idContacto \"([^\"]*)\"$")
    public void ejecutoServicioVerificarConTipoTipoidenDocumentoIdContacto(String tipo, String tipoiden, String documento, String idcontacto) {
        theActorCalled("robotdatos").attemptsTo(consumeCodigoVerificacionTask.conDatos(tipo,tipoiden,documento,idcontacto));
    }

    @Dado("^que se desea consumir el servicio para autenticarse$")
    public void queSeDeseaConsumirElServicioParaAutenticarse() {
        theActorCalled("robot").attemptsTo(consumeAutenticarTask.conDatos(System.getProperty("usuario"),System.getProperty("clave")));
    }
    @Cuando("^Ejecuto servicio verificar con tipo \"([^\"]*)\" tipoiden: \"([^\"]*)\" documento \"([^\"]*)\" campos1 \"([^\"]*)\" campos2 \"([^\"]*)\" campos3 \"([^\"]*)\" campos4 \"([^\"]*)\" campos5 \"([^\"]*)\"$")
    public void ejecutoServicioVerificarConTipoTipoidenDocumentoCamposCamposCamposCamposCampos(String tipo, String tipoiden, String documento, String campos1, String campos2, String campos3, String campos4, String campos5) {
        theActorCalled("robotdatos").attemptsTo(consumeTitularesTask.conDatos(tipo,tipoiden,documento,campos1,campos2,campos3,campos4,campos5));
    }
}