package co.com.solati.micovinoc.utils.hooks;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import io.restassured.specification.ProxySpecification;
import net.serenitybdd.rest.SerenityRest;
import net.serenitybdd.screenplay.actors.OnlineCast;
import net.serenitybdd.screenplay.rest.abilities.CallAnApi;

import static net.serenitybdd.screenplay.actors.OnStage.*;

public class consumeServicioHook {
    @Before
    public void preparaEndPoint(){
        setTheStage(new OnlineCast());
        theActorCalled("robot").whoCan(CallAnApi.at(System.getProperty("endpointAu")));
        theActorCalled("robotdatos").whoCan(CallAnApi.at(System.getProperty("endpointServ")));
        //SerenityRest.setDefaultProxy(new ProxySpecification("172.16.254.174",8000,"http"));
    }
    @After
    public void LiberarServicio(){drawTheCurtain();}
}
